resource "aws_api_gateway_rest_api" "my_api" {

  name = "my-api"

  description = "My API Gateway"

}

resource "aws_api_gateway_resource" "proxy" {

  rest_api_id = aws_api_gateway_rest_api.my_api.id

  parent_id = aws_api_gateway_rest_api.my_api.root_resource_id

  path_part = "mypath"

}

resource "aws_api_gateway_method" "proxy" {

  rest_api_id = aws_api_gateway_rest_api.my_api.id

  resource_id = aws_api_gateway_resource.proxy.id

  http_method = "ANY"

  authorization = "NONE"

}


resource "aws_api_gateway_integration" "lambda" {
  rest_api_id = aws_api_gateway_rest_api.my_api.id
  resource_id = aws_api_gateway_method.proxy.resource_id
  http_method = aws_api_gateway_method.proxy.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.this.invoke_arn
}


resource "aws_api_gateway_deployment" "example" {
  depends_on = [
    aws_api_gateway_integration.lambda,
  ]

  rest_api_id = aws_api_gateway_rest_api.my_api.id
  stage_name  = "test"
}

resource "aws_lambda_permission" "apigw" {
  statement_id = "AllowAPIGatewayInvoke"
  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.this.function_name
  principal = "apigateway.amazonaws.com"
  source_arn = "${aws_api_gateway_rest_api.my_api.execution_arn}/*/*"
}

resource "aws_api_gateway_api_key" "this" {
  name = "helloworld-locastack-test"
  description = "API Key for localstack test"
}

resource "aws_api_gateway_domain_name" "this" {
  domain_name = "helloworld.myapp.earth"
}